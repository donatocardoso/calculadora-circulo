const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: [
    path.join(__dirname, "/src/index.jsx")
  ],
  output: {
    filename: "build.js",
    path: path.join(__dirname, "/public")
  },
  module: {
    rules: [
      {
        test: /\.js(x)$/,
        exclude: /node_modules/,
        include: path.join(__dirname, "/src"),
        use: [
          "babel-loader"
        ]
      },
      {
        test: /\.css$/i,
        exclude: /node_modules/,
        include: path.join(__dirname, "/src"),
        use: [
          "style-loader",
          "css-loader"
        ],
      },
      {  
        test: /\.(png|jpg)$/,
        exclude: /node_modules/,
        include: path.join(__dirname, "/src"),
        use: [
          "file-loader"
        ]
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin ({
      template: path.join(__dirname, "/src/index.html")
    })
  ],
  target: "web",
  stats: "errors-only",
  devtool: "source-map",
  devServer: {
    port: 8080,
    compress: true,
    clientLogLevel: "none",
    watchContentBase: true,
    historyApiFallback: true,
    contentBase: path.join(__dirname, "dist")
  }
};
