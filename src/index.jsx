import "./Style.css";

import React from "react";
import ReactDOM from "react-dom";
import { createStore } from "redux";
import { Provider } from "react-redux";

import rootReducers from "./reducers/rootReducers";
import Calcular from "./template/calcular.jsx";

const store = createStore(rootReducers);

ReactDOM.render(
	<Provider store={store}>
		<div className="container">
			<Calcular />
		</div>
	</Provider>,
	document.getElementById("app"),
);
