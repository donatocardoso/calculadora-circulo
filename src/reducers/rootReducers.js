const initState = { raio: "", resposta: {}, showResposta: false };

export default (state = initState, action) => {
	switch (action.type) {
		case "RAIO_CHANGED":
			return { ...state, raio: action.payload };
		case "CALCULA":
			return {
				...state,
				resposta: {
					raio: action.payload.raio,
					comp: action.payload.comp,
					area: action.payload.area,
					vol: action.payload.vol,
				},
			};
		case "SHOW_RESPOSTA":
			return { ...state, showResposta: action.payload };
		default:
			return state;
	}
};
