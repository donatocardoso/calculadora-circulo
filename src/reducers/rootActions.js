export const handleChangeRaio = (event, val) => {
	return {
		type: "RAIO_CHANGED",
		payload: event ? event.target.value : val,
	};
};

export const handleCalc = raio => {
	return {
		type: "CALCULA",
		payload: {
			raio: raio,
			comp: (2 * 3.1415 * raio).toFixed(2),
			area: (3.1415 * Math.pow(raio, 2)).toFixed(2),
			vol: ((4 * 3.1415 * Math.pow(raio, 3)) / 3).toFixed(2),
		},
	};
};

export const handleShowResposta = showResposta => {
	return {
		type: "SHOW_RESPOSTA",
		payload: showResposta,
	};
};
