import { connect } from "react-redux";
import React, { Component } from "react";

import { handleChangeRaio } from "../reducers/rootActions";

class Resposta extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="divResposta">
				<label className="title">Resposta</label>

				<label className="subTitle">
					Número informado: {this.props.resposta.raio}
				</label>

				<table>
					<thead>
						<tr>
							<td>
								<label>Comprimento</label>
							</td>
							<td>
								<label>Área</label>
							</td>
							<td>
								<label>Volume</label>
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<label>{this.props.resposta.comp}</label>
							</td>
							<td>
								<label>{this.props.resposta.area}</label>
							</td>
							<td>
								<label>{this.props.resposta.vol}</label>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	raio: state.raio,
	showResposta: state.showResposta,
	resposta: state.resposta,
});

const mapDispatchToProps = {
	handleChangeRaio,
};

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Resposta);
