import { connect } from "react-redux";
import React, { Component } from "react";

import
{
	handleChangeRaio,
	handleCalc,
	handleShowResposta,
} from "../reducers/rootActions";

import Resposta from "./resposta.jsx";

class Calcular extends Component
{
	constructor(props)
	{
		super(props);
	}

	render()
	{
		return (
			<div className="smallModal">
				<label className="title">Donato Cardoso Ávila</label>

				<label className="subTitle">Informe o Raio do circulo:</label>

				<input
					type="number"
					className="inlineBlock"
					value={this.props.raio}
					onChange={this.props.handleChangeRaio}
					onKeyUp={this.handlePressKey.bind(this)}
				/>

				<button
					className="btnSubmit inlineBlock"
					onClick={this.handleCalc.bind(this)}
				>
					Enviar
				</button>

				{this.props.showResposta ? <Resposta /> : null}
			</div>
		);
	}

	handlePressKey(event)
	{
		if (event.key == "Enter") this.handleCalc();
	}

	handleCalc()
	{
		if (this.props.raio)
		{
			this.props.handleCalc(this.props.raio);
		} else
		{
			alert("Insira um valor...");
		}

		this.props.handleShowResposta(this.props.raio);
		this.props.handleChangeRaio(null, "");
	}
}

const mapStateToProps = state => ({
	raio: state.raio,
	showResposta: state.showResposta,
	resposta: state.resposta,
});

const mapDispatchToProps = {
	handleChangeRaio,
	handleCalc,
	handleShowResposta,
};

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Calcular);
